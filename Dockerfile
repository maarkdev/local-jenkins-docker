FROM jenkins/jenkins:latest
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
COPY --chown=jenkins:jenkins plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
#RUN jenkins-plugin-cli -f /usr/share/jenkins/ref/plugins.txt


#COPY conf/config.xml /var/jenkins_home/config.xml
#ENV CASC_JENKINS_CONFIG /var/jenkins_home/casc.yaml
#COPY casc.yaml /var/jenkins_home/casc.yaml
