# Dockernized Jenkins #

### What is this repository for? ###

This repository contains what is needed for the creation of a docker container with Jenkins. The Jenkins will be available on port 8080. Having aa dockerized Jenkins will allow for better manageability as well as integration into the environment going forward.  This now also mounts all plugins currently running on the current Jenkins server.

This should be run as the JENKINS user

### How do use it ###

1.      docker network create jenkins
2.      docker build -t jenkins:Maark .
3.	docker-compose up -d
4.      docker ps --filter name=Jenkins
5.      docker exec -it Jenkins-Maark bash

### To delete existing local Jenkins containers the following command should run ###
docker ps --filter name=Jenkins* -aq | xargs docker stop | xargs docker rm

### Docker run command just for reference ###
docker run --name Jenkins-Maark --detach \
-v /var/lib/jenkins/.ssh:/var/jenkins_home/.ssh \
-v /mnt/data/jenkins/jobs:/var/jenkins_home/jobs \
-v /mnt/data/jenkins/users:/var/jenkins_home/users \
-v /mnt/data/jenkins/secrets:/var/jenkins_home/secrets \
-v /mnt/data/jenkins/logs:/var/jenkins_home/logs \
-v /mnt/data/jenkins/plugins:/var/jenkins_home/plugins \
-v /mnt/data/jenkins/tools:/var/jenkins_home/tools \
-v /mnt/data/jenkins/workspace:/var/jenkins_home/workspace \
-p 8080:8080 jenkins:Maark
